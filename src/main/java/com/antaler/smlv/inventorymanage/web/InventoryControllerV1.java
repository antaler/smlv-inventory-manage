package com.antaler.smlv.inventorymanage.web;

import java.util.Map;

import org.apache.hc.core5.http.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.antaler.smlv.inventorymanage.kafka.KafkaPublisher;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("v1/inventory")
public class InventoryControllerV1 {


    @Autowired
    private KafkaPublisher kfk;

    @PostMapping("product")
    public Mono<ResponseEntity<Object>> addProduct(@RequestBody Map<String,String> body) throws NotImplementedException {
        return kfk.publishAddProduct(body.getOrDefault("name", "EMPTY")).map(b -> ResponseEntity.ok(b));

    }   

}
