package com.antaler.smlv.inventorymanage.model.dto;

public record ProductDTO(String name, String type) {
    
}
