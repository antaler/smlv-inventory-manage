package com.antaler.smlv.inventorymanage.kafka;

import java.util.Map;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;

@Service
public class KafkaPublisher {
    
    private final KafkaTemplate<String,String> kafkaTemplate;

    public KafkaPublisher(KafkaTemplate<String,String> template){
        this.kafkaTemplate = template;
    }

    public Mono<Object> publishAddProduct(String product) {
        return Mono.fromFuture(this.kafkaTemplate.send("add_product", product)).handle((result, sink) -> {
            System.out.println(String.format("Send add Product %s with offset %d", product, result.getRecordMetadata().offset()));
            sink.next(Map.of("offset", String.valueOf(result.getRecordMetadata().offset())));
        }).doOnError(err -> {System.out.println(String.format("Error %s", err.getMessage())); }).onErrorReturn(Map.of("error", "Error con kafka"));


    }



}
